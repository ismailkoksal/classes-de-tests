<?php

use PHPUnit\Framework\TestCase;

include '..\Class\Article.class.php';
include '..\Class\Auteur.class.php';
include '..\Class\Image.class.php';

class testArticle extends TestCase
{
    public function testCreationArticle()
    {
        $unAuteur = new Auteur("Doe", "John");
        $unArticle = new Article('t', 'c', $unAuteur);
        $this->assertNotNull($unArticle, "Article non instancié");
        $this->assertSame("t", $unArticle->getTitre());
        $this->assertSame("c", $unArticle->getContenu());
        $this->assertSame("<h3> t </h3>  <p>c</p><h6> Doe John</h6>", $unArticle->__toString());
    }

    public function testAjoutAuteur()
    {
        //créer un auteur et l'article et vérifier que l'auteur s'insère bien dans l'article
        $unAuteur = new Auteur('Doe', 'John');
        $this->assertNotNull($unAuteur, "Auteur non instancié");
        $this->assertSame("Doe", $unAuteur->getNom());
        $this->assertSame("John", $unAuteur->getPrenom());
        $unArticle = new Article('t', 'c', $unAuteur);
        $this->assertSame("<h6> Doe John</h6>", $unArticle->getAuteur());
    }

    public function testNbImage()
    {
        //tester la méthode nbimages() en vérifiant qu'à la création d'un article, il n'y a bien aucune image d'insérée.
        $unAuteur = new Auteur('Doe', 'John');
        $unArticle = new Article('t', 'c', $unAuteur);
        $this->assertSame(0, $unArticle->nbImages());
    }

    public function testAjouterImage()
    {
        //tester la méthode ajoutImage()
        // en vérifiant qu'à la création d'un article,
        // celle-ci est bien ajoutée dans l'article
        $unAuteur = new Auteur('Doe', 'John');
        $unArticle = new Article('t', 'c', $unAuteur);
        $uneImage = new Image('legende', 'nom');
        $unArticle->ajouterImage($uneImage);
        $this->assertSame(1, $unArticle->nbImages());

    }

    public function testSupprimerImage()
    {
        //tester la méthode supprimerImage() en vérifiant qu'une image est bien supprimée de l'article
        $unAuteur = new Auteur('Doe', 'John');
        $unArticle = new Article('t', 'c', $unAuteur);
        $uneImage = new Image('legende', 'nom');
        $unArticle->ajouterImage($uneImage);
        $this->assertSame(1, $unArticle->nbImages());
        $unArticle->SupprimerImage($uneImage);
        $this->assertSame(0, $unArticle->nbImages());
    }

    public function testModifierImage()
    {
        //tester la méthode ModifierImage() en vérifiant qu'une image est bien modifiée dans l'article
        $unAuteur = new Auteur('Doe', 'John');
        $unArticle = new Article('t', 'c', $unAuteur);
        $uneImage = new Image('legende', 'nom');
        $unArticle->ajouterImage($uneImage);
        $this->assertSame("<h3> legende nom</h3>", $unArticle->getImages());
        $unArticle->ModifierImage($uneImage, "l", "n");
        $this->assertSame("<h3> l n</h3>", $unArticle->getImages());
    }

}