<?php

use PHPUnit\Framework\TestCase;

include '..\Class\Auteur.class.php';

class testAuteur extends TestCase
{
    public function testCompareNomAuteur()
    {
        Auteur::$choixTRIAuteur = 1;
        $date1 = new DateTime("2000-12-01");
        $date2 = new DateTime("2000-12-12");
        $date3 = new DateTime("1999-04-19");
        $date4 = new DateTime("1999-04-19");
        $premierAuteur = new Auteur("BERNARD", "Jean", $date1);
        $deuxiemeAuteur = new Auteur("BIANCA", "Bella", $date2);
        $troisiemeAuteur = new Auteur("BIANCA", "Antonio", $date3);
        $quatriemeAuteur = new Auteur("XESTIG", "Jean", $date4);
        $this->assertSame(-1, $premierAuteur->compareTo($deuxiemeAuteur), $premierAuteur->getNom()." doit être avant alphabétiquement ".$deuxiemeAuteur->getNom());
        $this->assertSame(0, $deuxiemeAuteur->compareTo($troisiemeAuteur), $deuxiemeAuteur->getNom()." doit être identique ".$troisiemeAuteur->getNom());
        $this->assertSame(1, $quatriemeAuteur->compareTo($troisiemeAuteur), $quatriemeAuteur->getNom()." doit être après alphabétiquement ".$troisiemeAuteur->getNom());
    }

    public function testComparePrenomAuteur()
    {
        Auteur::$choixTRIAuteur = 2;
        $date1 = new DateTime("2000-12-01");
        $date2 = new DateTime("2000-12-12");
        $date3 = new DateTime("1999-04-19");
        $date4 = new DateTime("1999-04-19");
        $premierAuteur = new Auteur("BERNARD", "Jean", $date1);
        $deuxiemeAuteur = new Auteur("BIANCA", "Bella", $date2);
        $troisiemeAuteur = new Auteur("BIANCA", "Antonio", $date3);
        $quatriemeAuteur = new Auteur("XESTIG", "Jean", $date4);
        $this->assertSame(-1, $troisiemeAuteur->compareTo($deuxiemeAuteur), $troisiemeAuteur->getPrenom()." doit être avant alphabétiquement ".$deuxiemeAuteur->getPrenom());
        $this->assertSame(0, $premierAuteur->compareTo($quatriemeAuteur), $premierAuteur->getPrenom()." doit être identique ".$quatriemeAuteur->getPrenom());
        $this->assertSame(1, $quatriemeAuteur->compareTo($troisiemeAuteur), $quatriemeAuteur->getPrenom()." doit être après alphabétiquement ".$troisiemeAuteur->getPrenom());
    }

    public function testCompareDateAuteur()
    {
        Auteur::$choixTRIAuteur = 3;
        $date1 = new DateTime("2000-12-01");
        $date2 = new DateTime("2000-12-12");
        $date3 = new DateTime("1999-04-19");
        $date4 = new DateTime("1999-04-19");
        $premierAuteur = new Auteur("BERNARD", "Jean", $date1);
        $deuxiemeAuteur = new Auteur("BIANCA", "Bella", $date2);
        $troisiemeAuteur = new Auteur("BIANCA", "Antonio", $date3);
        $quatriemeAuteur = new Auteur("XESTIG", "Jean", $date4);
        $this->assertSame(-1, $troisiemeAuteur->compareTo($deuxiemeAuteur), date_format($troisiemeAuteur->getDateNaiss(), 'Y-m-d')." doit être avant alphabétiquement ".date_format($deuxiemeAuteur->getDateNaiss(), 'Y-m-d)'));
        $this->assertSame(0, $troisiemeAuteur->compareTo($quatriemeAuteur), date_format($troisiemeAuteur->getDateNaiss(), 'Y-m-d')." doit être identique ".date_format($quatriemeAuteur->getDateNaiss(), 'Y-m-d'));
        $this->assertSame(1, $deuxiemeAuteur->compareTo($premierAuteur), date_format($deuxiemeAuteur->getDateNaiss(), 'Y-m-d')." doit être après alphabétiquement ".date_format($premierAuteur->getDateNaiss(), 'Y-m-d'));
    }
}