<?php

use PHPUnit\Framework\TestCase;

include '..\Class\Magazine.class.php';
include '..\Class\Article.class.php';

class testMagazine extends TestCase
{
    public function testCreationMagazine()
    {
        $infoBoulotDodo = new Magazine(1, "10/09/2018");
        $this->assertNotNull($infoBoulotDodo, "Magazine non instancié");
        $this->assertSame(1, $infoBoulotDodo->getNumero(), "erreur dans la récupération du numéro du magazine");
        $this->assertSame("10/09/2018", $infoBoulotDodo->getdate());
        $this->assertSame(0, $infoBoulotDodo->getNbPages());
    }

    public function testNbArticle()
    {
        //tester la méthode nbArticles() en vérifiant qu'à la création d'un magazine, il n'y a bien aucun article.
        $infoBoulotDodo = new Magazine(2, "12/11/2018");
        $this->assertNotNull($infoBoulotDodo, "Magazine non instancié");
        $this->assertSame(0, $infoBoulotDodo->nbArticles(), "erreur il y a un article");
    }

    public function testAjoutArticle()
    {
        //tester la méthode ajoutArticle() de la classe Magazine
        // en vérifiant qu'à la création d'un magazine et d'un article,
        // celui-ci est bien ajouté dans le magazine ( dans la collection lesarticles)
        $infoBoulotDodo = new Magazine(3, "12/11/2018");
        $this->assertNotNull($infoBoulotDodo, "Magazine non instancié");
        $unArticle = new Article("t", "c", "a");
        $this->assertNotNull($unArticle, "Article non instancié");
        $infoBoulotDodo->ajouterArticle($unArticle);
        $this->assertSame(1, $infoBoulotDodo->getNbPages(), "erreur dans l'ajout de l'article");


    }

    public function testPrix()
    {
        //tester la méthode prix() de la classe Magazine
        $infoBoulotDodo = new Magazine(3, "12/11/2018");
        $this->assertNotNull($infoBoulotDodo, "Magazine non instancié");
        $this->assertSame(2, $infoBoulotDodo->prix());

        //créer le magazine avec ses articles
        $unArticle = new Article("t", "c", "a");
        $this->assertNotNull($unArticle, "Article non instancié");
        $infoBoulotDodo->ajouterArticle($unArticle);
        $this->assertSame(2.2, $infoBoulotDodo->prix());

        $unArticle2 = new Article("t2", "c2", "a2");
        $this->assertNotNull($unArticle2, "Article non instancié");
        $infoBoulotDodo->ajouterArticle($unArticle2);
        $this->assertSame(2.4, $infoBoulotDodo->prix());

        $unArticle3 = new Article("t3", "c3", "a3");
        $this->assertNotNull($unArticle3, "Article non instancié");
        $infoBoulotDodo->ajouterArticle($unArticle3);
        $this->assertSame(2.6, $infoBoulotDodo->prix());

    }

}