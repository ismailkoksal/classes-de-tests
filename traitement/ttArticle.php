<?php
session_start();
include '../Class/Magazine.class.php';
include '../Class/Article.class.php';
include '../Class/Auteur.class.php';
include '../Class/Image.class.php';
include '../Class/Outils.class.php'; //classe contenant une méthode STATIQUE
include '../Class/ArticleInterview.class.php';

if (isset($_SESSION["ObjetMagazine"])) {

    //récupération de l'objet Magazine en le désérialisant !
    $infoBoulotDodo = unserialize($_SESSION["ObjetMagazine"]);
    Outils::console_log("voici mon OBJET magazine récupéré après désérialisation : </br>");
    Outils::console_log($infoBoulotDodo); //voici mon objet récupéré via la session
    echo "</br></br>";
    $ry = new Auteur("REZA", "Yasmina", "17/12/2018");
    $dp = new Auteur("DUPONT", "Pierre", "17/12/2018");
    $ik = new Auteur('KOKSAL', "Ismail", "17/12/2018");
    $image = new Image("photo de la mer", "../photos/mer.jpg");

    $a1 = new Article("RGPD : 3 mois après des entreprises témoignent", "Trois mois se sont écoulés depuis l'entrée en vigueur du RGPD en France ! Le but de cette enquête n'est pas de rappeler les énièmes obligations du RGPD ou de faire un inventaire des solutions certifiées mais de tirer un premier bilan dans la mise en oeuvre de ce règlement européen sur les données personnelles dans les entreprises. Nous avons donc choisi de vous faire partager quelques témoignages de sociétés qui ont déjà mené un certain nombre d'actions. Plus généralement, depuis ces trois mois, de nombreuses plaintes ont été déposé auprès de la Cnil surtout contre les GAFAM et de nouvelles pratiques et usages voient le jour dans les entreprises, tous les collaborateurs, globalement bien sensibilisés à en croire nos témoins, vont devoir s'y soumettre.", $ry);

    $infoBoulotDodo->ajouterArticle($a1);
    $a2 = new Article("Les mauvaises manières de la communauté Linux découragent les développeurs", "Voilà des années que la développeuse Sarah Sharp, salariée d'Intel, se plaignait des comportements abusifs de la communauté Linux. Elle avait décidé, il y a deux ans de mettre un terme à ses contributions directes avec le groupe de développeurs du noyau. Aujourd'hui, la communauté Linux annonce adopter un code contre les conduites abusives.", $dp);
    $infoBoulotDodo->ajouterArticle($a2);

    $a3 = new ArticleInterview("Victor le bg", "Vendredi tacos", $ik, "Victor LeFrancois", "Tacos");
    $infoBoulotDodo->ajouterArticle($a3);

    echo "<h1>le nombre d'article est " . $infoBoulotDodo->nbArticles() . "</h1>";
    echo($infoBoulotDodo); //voici mon objet avec mon nouvel article
    echo "</br></br>";


    Outils::console_log("voici mon OBJET magazine SERIALISE pour le RE-stocker en SESSION: </br>");
    //stockage de l'objet magazine "$infoBoulotDodo" dans la session afin de
    $serializedMag = serialize($infoBoulotDodo); // On serialize et on stocke cette chaîne.
    $_SESSION["ObjetMagazine"] = $serializedMag;
    Outils::console_log($_SESSION); //vérifier le stockage de l'objet
} else {
    echo "le magazine n'a pas encore été créé ! <a href='ttMagazine.php'>cliquez ici pour le créer !</a></br></br>";
}
?>