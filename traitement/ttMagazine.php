<?php
session_start();
include '../Class/Magazine.class.php';
include '../Class/Article.class.php';
include '../Class/Outils.class.php'; //classe contenant une méthode STATIQUE

$infoBoulotDodo = new Magazine(1, "10/09/2018");

echo($infoBoulotDodo); //voici mon objet Magazine (le toString s'appelle  automatiquement)

//appel de la méthode Statique console_log de la classe Outils
Outils::console_log("<h1>DEBUG</h1> : voici mon OBJET magazine SERIALISE pour le stocker en SESSION: </br>");
$serializedMag = serialize($infoBoulotDodo); // On serialize et on stocke cet objet.
$_SESSION["ObjetMagazine"] = $serializedMag;//stockage de l'objet magazine "$infoBoulotDodo" dans la session
Outils::console_log($_SESSION); //vérifier le stockage de l'objet dans la session*/

echo "BRAVO, le magazine N°".$infoBoulotDodo->getNumero()."est créé ! <a href='../traitement/ttArticle.php'>cliquez ici pour y AJOUTER des ARTICLES !</a></br></br>";




?>
