<?php

interface Comparer
{
    public function compareTo($objet) : int;

}