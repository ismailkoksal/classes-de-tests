<?php

class coArticle extends Article
{
    private $contenuCoAuteur;
    private $coAuteur;

    public function __construct($t, $c, $a, $contenuCoAuteur, $coAuteur)
    {
        parent::__construct($t, $c, $a);
        $this->contenuCoAuteur = $contenuCoAuteur;
        $this->coAuteur = $coAuteur;
    }
}