<?php
/**
 * Created by PhpStorm.
 * User: ChevrollierBtsSIO1
 * Date: 15/10/2018
 * Time: 15:29
 */

class Image
{
    /**
     * Classe Image permettant de créer une Image
     *
     * variables d'instances
     * @var string $legende : légende de l'image
     * @var string $nom : nom de l'image
     */

    private $legende;
    private $nom;

    public function __construct($l, $n)
    {
        $this->legende = $l;
        $this->nom = $n;

    }

    public function getLegende()
    {
        return $this->legende;
    }

    public function setLegende($value)
    {
        $this->legende = $value;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($value)
    {
        $this->nom = $value;
    }


    /**
     * méthode toString qui permet de retourner une chaine contenant les informations de l'image
     * ici cette chaine est formatée en HTML pour un affichage plus convivial
     * @return $chaine
     */
    public function __toString()
    {
        $chaine = "<h3> " . $this->getLegende() . " " . $this->getNom() . "</h3>";
        return $chaine;
    }
}

?>
}