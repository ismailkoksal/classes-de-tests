<?php

class ArticleInterview extends Article {
    private $nomInterview;
    private $caracteristiquesInterview;
    private $lesQuestions;

    public function __construct($t, $c, $a, $nomInterview, $caracteristiquesInterview)
    {
        parent::__construct($t, $c, $a);
        $this->nomInterview = $nomInterview;
        $this->caracteristiquesInterview = $caracteristiquesInterview;
        $this->lesQuestions = array();
    }

    public function ajouterQuestion($uneQuestion) {
        array_push($this->lesQuestions, $uneQuestion);
    }

    public function ajouterReponse($uneQuestion, $uneReponse) {
        $numQuestion = array_search($uneQuestion, $this->lesQuestions);
        $this->lesQuestions[$numQuestion]->setReponse($uneReponse);
    }

    public function getNbQuestions() {
        return count($this->lesQuestions);
    }
}