<?php

class Question {
    private $libelleQuestion;
    private $reponseInterview;

    public function __construct($libelleQuestion, $reponseInterview = null) {
        $this->libelleQuestion = $libelleQuestion;
        if ($reponseInterview != null) {
            $this->reponseInterview = $reponseInterview;
        }
    }

    public function setReponse($value) {
        $this->reponseInterview = $value;
    }
}