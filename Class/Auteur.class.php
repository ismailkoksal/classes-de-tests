<?php

include 'Comparer.php';

class Auteur implements Comparer
{
    /**
     * Classe Auteur permettant de créer un auteur
     *
     * variables d'instances
     * @var chaine $nom : nom de l'auteur
     * @var chaine $prenom : prenom de l'auteur
     */

    private $nom;
    private $prenom;
    private $dateNaiss;
    public static $choixTRIAuteur = 1;
    //1 si tri par nom PAR DEFAUT
    //2 si tri par prénom
    //3 si tri par date de naissance

    public function __construct($n, $p, $dn)
    {
        $this->nom = $n;
        $this->prenom = $p;
        $this->dateNaiss = $dn;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getDateNaiss()
    {
        return $this->dateNaiss;
    }


    /**
     * méthode toString qui permet de retourner une chaine contenant tous les informations de l'auteur
     * ici cette chaine est formatée en HTML pour un affichage plus convivial
     * @return $chaine
     */
    public function __toString()
    {
        $chaine = "<h6> " . $this->getNom() . " " . $this->getPrenom() . "</h6>";
        return $chaine;
    }

    /** méthode CompareTo
     * @uses $choixTRIAuteur // variable static pour connaître le TRI à faire
     *      // 1 si tri par nom     // 2 si tri par prénom      // 3 si tri par date de naissance
     * @param $auteur
     * @return int // retourne une valeur (cas du tri par nom)
     * -1 si cet auteur a un nom AVANT alphabétiquement à l'autre auteur ($auteur),
     * 1 si cet auteur a un nom APRES alphabétiquement à l'autre auteur ($auteur),
     * 0 si cet auteur a un nom IDENTIQUE a l'autre auteur ($auteur),
     */
    public function compareTo($auteur): int
    {
        if (self::$choixTRIAuteur == 1) {
            return strcmp($this->nom, $auteur->nom);
        }
        if (self::$choixTRIAuteur == 2) {
            return strcmp($this->prenom, $auteur->prenom);
        }
        if (self::$choixTRIAuteur == 3) {
            if ($this->dateNaiss < $auteur->dateNaiss) {
                return -1;
            }
            if ($this->dateNaiss > $auteur->dateNaiss) {
                return 1;
            }
            return 0;
        }
    }
}