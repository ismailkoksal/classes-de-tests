<?php

/**
 * Classe Article permettant de créer un article
 *
 * variables d'instances
 * @var chaine $titre : titre de l'article
 * @var chaine $contenu : contenu de l'article
 * @var Auteur $auteur : auteur doit être défini à la création de l'article (lien 1)
 * @var Image $lesImages : liste des images de l'article
 */
class Article
{
    private $titre; //titre de l'article
    private $contenu; //le contenu peut contenir ou non des images
    private $auteur; //l'auteur doit être défini à la création de l'article (lien 1)
    private $lesImages; //l'auteur doit être défini à la création de l'article (lien 1)

    public function __construct($t, $c, $a)
    {
        $this->titre = $t;
        $this->contenu = $c;
        $this->auteur = $a;
        $this->lesImages = array();
    }

    public function getTitre()
    {
        return $this->titre;
    }

    public function getContenu()
        //affiche le contenu ainsi que les imagees
    {
       $retour = $this->contenu ;
       if ($this->getImages() != null){
           $retour += $this->getImages();
       }
        return $retour;
    }

    public function getAuteur()
    {
        return $this->auteur->__toString();
    }

    public function getImages()
    {
        $listeImage = null;
        foreach ($this->lesImages as $uneImage){

            $listeImage .= $uneImage->__toString();
    }
        return $listeImage;
    }
    public function nbImages()
    {
        return count($this->lesImages);
    }
    public function ajouterImage($img)
    {
        //todo 1 : écrire la fonction ajouterImage en ajoutant l'objet image initialement créé
        array_push($this->lesImages, $img);

    }
    public function SupprimerImage($img)
    {
        //todo 2 : écrire la fonction supprimerImage à  partir du numéro de l'image
        $numImage = array_search($img, $this->lesImages);
        unset($this->lesImages[$numImage]);
        $this->lesImages = array_merge($this->lesImages);
    }
    public function ModifierImage($img, $legende, $nom)
    {
        //todo 3 : écrire la fonction modifierImage à partir du numéro de l'image
        $numImage = array_search($img, $this->lesImages);
        $this->lesImages[$numImage]->setLegende($legende);
        $this->lesImages[$numImage]->setNom($nom);
    }
    /**
     * méthode toString qui permet de retourner une chaine contenant tous les éléments de l'article
     * ici cette chaine est formatée en HTML pour un affichage plus convivial
     * @return $chaine
     */
    public function __toString()
    {
        $chaine = "<h3> ".$this->getTitre()." </h3>  <p>".$this->getContenu()."</p>".$this->getAuteur();
        return $chaine;
    }
}