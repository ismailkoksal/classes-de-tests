<?php
/**
 * Created by PhpStorm.
 * User: ChevrollierBtsSIO1
 * Date: 27/09/2018
 * Time: 17:22
 */

class Outils
{
//petite fonction pour afficher les DEBUGS dans la console
    STATIC function console_log($data)
    {
        echo '<script>';
        echo 'console.log(' . json_encode($data) . ')';
        echo '</script>';
    }
}