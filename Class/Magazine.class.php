<?php

/**
 * Classe Magazine permettant de créer le magazine et d'y ajouter des articles
 *
 * variables d'instances
 * @var int $numero : numéro du magazine
 * @var date $date : date de création du magazine
 * @var int $nbpages : nombre de pages du magazine non limitée (1 article = 1 page)
 * @var collection d'Articles $lesArticles : liste des articles présents dans le magazine
 *
 */
class Magazine
{

    private $numero;
    private $date;
    private $nbPages;
    private $lesArticles;

    public function __construct($n, $d)
    {
        $this->numero = $n;
        $this->date = $d;
        $this->nbPages = 0;
        $this->lesArticles = array();
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getNbPages()
    {
        return $this->nbPages;
    }

    /**
     * méthode ajouterArticle qui permet d'ajouter un article au magazine
     * @param  Article $unArticle
     */
    public function ajouterArticle($unArticle)
    {
        $this->lesArticles[] = $unArticle;

        if ($unArticle instanceof ArticleInterview) {
            $nbPages = ($unArticle->getNbQuestions()) % 5;
            $this->nbPages += $nbPages;
        } else {
            $this->nbPages++;
        }
    }

    /**
     * méthode supprimerArticle qui supprime l'article à la page indiquée
     * @param int $p : numéro de la page contenant l'article (comme ici un article est sur une seule page)
     */
    public function supprimerArticleParPage($p)
    {
        unset($this->lesArticles[$p]);
        $this->nbPages--;
    }

    /**
     * méthode supprimerArticle qui supprime l'article en passant l'objet
     * @param Article $lArticle : numéro de la page contenant l'article
     */
    public function supprimerArticle($lArticle)
    {
        $indice = array_search($lArticle, $this->lesArticles);
        $this->supprimerArticleParPage($indice);
    }

    /**
     * méthode nbArticles qui retourne le nombre d'articles du magazine
     * @return int d'articles
     */
    public function nbArticles()
    {
        return count($this->lesArticles);
    }

    /**
     * méthode toString qui permet de retourner une chaine contenant TOUTES les données du MAGAZINE
     *  ici cette chaine est formatée en HTML pour un affichage plus convivial
     * @return $chaine
     */
    public function __toString()
    {
        $chaine = "<h1>Le " . $this->getDate() . " - magazine N°" . $this->getNumero() . " </h1>";
        foreach ($this->lesArticles as $a) {
            $chaine .= $a; //la méthode toString sur l'objet Article $a est appelée automatiquement
        }
        return $chaine;
    }

    public function prix()
    {

        //la méthode prix() de la classe Magazine
        // prix du magazine est calculé avec un tarif fixe de 2€ + 20 centimes par page
        $prix = 2;
        for ($i = 1; $i <= $this->nbPages; $i++) {
            $prix += 0.2;
        }
        return $prix;
    }

}

?>